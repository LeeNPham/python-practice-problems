# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!


def safe_divide(value1, value2):
    if value2 == 0:
        return "math.inf"

    return value1 / value2


print(safe_divide(15, 3))
