# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

values = [99, 98, 55, 89, 64]


def calculate_grade(values):
    score_average = sum(values) / len(values)
    if score_average >= 90:
        return "A"
    elif score_average >= 80:
        return "B"
    elif score_average >= 70:
        return "C"
    elif score_average >= 60:
        return "D"
    else:
        return "F"


print(calculate_grade(values))
